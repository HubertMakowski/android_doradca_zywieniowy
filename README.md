# android_doradca_zywieniowy
This application is made for Android mobile phone's.

<a href="https://imgflip.com/gif/284oqc"><img src="https://i.imgflip.com/284oqc.gif" title="made at imgflip.com"/></a>

# Tools and technologies used:
* [Model View Presenter](https://msdn.microsoft.com/en-us/library/ff649571.aspx)
* [Firebase](https://firebase.google.com/)
* [SQLite](https://www.sqlite.org/index.html)
* [Stetho](http://facebook.github.io/stetho/)
* [Butter Knife](http://jakewharton.github.io/butterknife/)
