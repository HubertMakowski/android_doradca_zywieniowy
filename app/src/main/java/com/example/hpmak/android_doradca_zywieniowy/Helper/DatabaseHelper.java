package com.example.hpmak.android_doradca_zywieniowy.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "AdvicesOffline.db";
    private static final int DATABASE_VERSION = 1;

    public static final String ADVICE = "ADVICE";

    private static final String ID = "ID";
    private static final String ADVICE_ID = "ADVICE_ID";
    private static final String ADVICE_IMAGE_URL = "IMAGE_URL";
    private static final String ADVICE_TITLE = "TITLE";
    private static final String ADVICE_DESCRIPTION = "DESCRIPTION";
    private static final String ADVICE_CATEGORY = "CATEGORY";
    private static final String ADVICE_TAG = "TAG";
    private static final String ADVICE_USER_UID = "USER_UID";

    private static final String tableAdvice = "create table "+ ADVICE +" ( "+ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "                                                              "+ ADVICE_ID +" TEXT NOT NULL, "
                                                                            +ADVICE_IMAGE_URL+" TEXT NOT NULL, "
                                                                            +ADVICE_TITLE+" TEXT NOT NULL, "
                                                                            +ADVICE_DESCRIPTION+" TEXT NOT NULL, "
                                                                            +ADVICE_CATEGORY+" TEXT NOT NULL, "
                                                                            +ADVICE_TAG+" TEXT NOT NULL," +
            "                                                              "+ADVICE_USER_UID+" TEXT);";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS "+ ADVICE);
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tableAdvice);
    }

    public boolean deleteDatabase(Context context) {
        return context.deleteDatabase(DATABASE_NAME);
    }
}
