package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.FirebaseAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnUploadDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.google.firebase.storage.UploadTask;

import java.util.Random;
import java.util.UUID;

public class AdviceActivityPresenter {

    private static String TAG = "AdviceActivityPresenter";
    private AdviceActivityView view;

    private FirebaseAdapter firebaseAdapter;

    public AdviceActivityPresenter(AdviceActivityView view) {
        this.view = view;
        firebaseAdapter = new FirebaseAdapter((Activity) view);
    }

    public interface AdviceActivityView {
        void goToMainScreen();
        String getMyTitle();
        String getDescription();
        String getCategory();
        String getTag();
    }

    //replace image in data storage and edit actual advice
    public void replaceImage(Uri uri, final Advice advice) {
        if(uri != null) {
            firebaseAdapter.getStorage().child("images/" + advice.getId()).delete();

            firebaseAdapter.setUri(uri);
            firebaseAdapter.uploadData("images/" + advice.getId(), new OnUploadDataListener() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    String imageUrl = String.valueOf(taskSnapshot.getDownloadUrl());
                    advice.setImageUrl(imageUrl);
                    firebaseAdapter.updateAdvice(advice);
                    view.goToMainScreen();
                }

                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.d(TAG, "onFailure: " + exception.getMessage());
                }
            });
        } else {
            firebaseAdapter.updateAdvice(advice);
            view.goToMainScreen();
        }
    }

    //upload image to data storage and add new advice
    public void uploadImage(Uri uri) {
        final String idAdvice = getNewId();

        if(uri != null) {
            firebaseAdapter.setUri(uri);
            firebaseAdapter.uploadData("images/" + idAdvice, new OnUploadDataListener() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    String imageUrl = String.valueOf(taskSnapshot.getDownloadUrl());
                    firebaseAdapter.addAdvice(new Advice(idAdvice, imageUrl, view.getMyTitle(), view.getDescription(), view.getCategory(), view.getTag()));
                    view.goToMainScreen();
                }

                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.d(TAG, "onFailure: " + exception.getMessage());
                }
            });
        }
    }

    private String getNewId() {
//        Random random = new Random();
//        final int idAdvice = random.nextInt(100000+1);
//        return String.valueOf(idAdvice);
        return String.valueOf(System.currentTimeMillis() / 1000);
//        return UUID.randomUUID().toString();
    }
}
