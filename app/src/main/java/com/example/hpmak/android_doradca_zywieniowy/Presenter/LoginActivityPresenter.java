package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.FirebaseAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Model.User;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.example.hpmak.android_doradca_zywieniowy.View.Activity.LoginActivity;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.concurrent.Executor;

import static android.content.Context.MODE_PRIVATE;

public class LoginActivityPresenter {

    private LoginActivityView view;
    private FirebaseAuth mAuth;

    public LoginActivityPresenter(LoginActivityView view) {
        this.view = view;
        mAuth = FirebaseAuth.getInstance();
    }

    public interface LoginActivityView {
        void updateUI(FirebaseUser user);

        void printMessage(String toast, String debug);

        void showUserEmailError(int resId);

        void showUserPasswordError(int resId);

        void goToMainScreen();

        String getUserEmail();

        String getUserPassword();
    }

    public boolean validateData() {
        String email = view.getUserEmail();
        String password = view.getUserPassword();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.showUserEmailError(R.string.user_email_error);
            return false;
        }
        if (password.isEmpty() || password.length() < 5) {
            view.showUserPasswordError(R.string.user_password_error);
            return false;
        }

        return true;
    }

    public void firebaseAuthWithEmail(final String email, String password) {
        final ProgressDialog progressDialog = new ProgressDialog((Activity) view);
        progressDialog.setMessage("Trwa logowanie...");
        progressDialog.show();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            progressDialog.dismiss();
                            view.updateUI(user);
                        } else {
                            view.printMessage("Podany e-mail jest już zarejestrowany w aplikacji", "firebaseAuthWithEmail()");
                            progressDialog.dismiss();
                            view.updateUI(null);
                        }
                    }
                });
    }

    public void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) view, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            view.updateUI(user);
                        } else {
                            view.printMessage("Podany e-mail jest już zarejestrowany w aplikacji", "handleFacebookAccessToken()");
                            view.updateUI(null);
                        }
                    }
                });
    }

    public void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) view, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            view.updateUI(user);
                        } else {
                            view.printMessage("Podany e-mail jest już zarejestrowany w aplikacji", "firebaseAuthWithGoogle()");
                            view.updateUI(null);
                        }
                    }
                });
    }

    public FirebaseAuth getAuth() {
        return mAuth;
    }
}
