package com.example.hpmak.android_doradca_zywieniowy.Listener;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

public interface OnGetDataListener {
    void onSuccess(DataSnapshot data);
    void onFailed(DatabaseError databaseError);
}
