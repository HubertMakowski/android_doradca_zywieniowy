package com.example.hpmak.android_doradca_zywieniowy.Model;

public class UserRatings {

    private String idAdvice;

    private int rating;
    private boolean isRead;

    public UserRatings() {}

    public UserRatings(String idAdvice, int rating, boolean isRead) {
        this.idAdvice = idAdvice;
        this.rating = rating;
        this.isRead = isRead;
    }

    @Override
    public String toString() {
        return "UserRatings{" +
                "idAdvice='" + idAdvice + '\'' +
                ", rating=" + rating +
                ", isRead=" + isRead +
                '}';
    }

    public String getIdAdvice() {
        return idAdvice;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
