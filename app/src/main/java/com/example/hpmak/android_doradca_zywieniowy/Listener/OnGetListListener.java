package com.example.hpmak.android_doradca_zywieniowy.Listener;

import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Model.UserRatings;

import java.util.List;
import java.util.Map;

public interface OnGetListListener {
    void onComplete(Map<String, List<Advice>> mapListAdvice,
                    List<UserRatings> listUserRatings);

    void onFailed(String error);
}
