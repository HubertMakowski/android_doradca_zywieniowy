package com.example.hpmak.android_doradca_zywieniowy.View.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.AdviceAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Model.UserRatings;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.example.hpmak.android_doradca_zywieniowy.View.Activity.MainActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NutritionFragment extends Fragment {

    @BindView(R.id.content_main_nutrition_lv)
    ListView listView;
    @BindView(R.id.content_main_nutrition_tv_number)
    TextView numberOfNotifications;
    @BindView(R.id.content_main_nutrition_iv_notification)
    ImageView notification;

    private List<UserRatings> listRatings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.content_main_nutrition, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getActivity() != null) {
            AdviceAdapter adapter = new AdviceAdapter(getContext(), 0, ((MainActivity)getActivity()).getListNutrition());
            listView.setAdapter(adapter);
        }
    }
}
