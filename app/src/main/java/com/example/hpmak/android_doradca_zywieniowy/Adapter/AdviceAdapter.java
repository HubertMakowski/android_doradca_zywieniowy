package com.example.hpmak.android_doradca_zywieniowy.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Model.UserRatings;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.SavedActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.example.hpmak.android_doradca_zywieniowy.View.Activity.AdviceActivity;
import com.example.hpmak.android_doradca_zywieniowy.View.ExpandAdviceView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdviceAdapter extends ArrayAdapter<Advice> {

    private final String TAG = "AdviceAdapter";

    private List<Advice> advices = null;
    private List<UserRatings> ratings = null;

    private DatabaseAdapter database;
    private FirebaseAdapter firebaseAdapter;

    private String searchText;

    private Boolean isMainActivity = true;
    private SavedActivityPresenter.SavedActivityView view;

    //for heart optimization
    private boolean isFirstCall;
    private boolean[] states;

    @BindView(R.id.custom_advice_ll_clickable)
    LinearLayout linearLayout;
    @BindView(R.id.custom_advice_ll_advice)
    LinearLayout layoutAdvice;
    @BindView(R.id.custom_advice_iv_photo)
    ImageView image;
    @BindView(R.id.custom_advice_tv_title)
    TextView title;
    @BindView(R.id.custom_advice_tv_desc)
    TextView desc;
    @BindView(R.id.custom_advice_iv_heart)
    ImageView iconHeart;
    @BindView(R.id.custom_advice_tv_rating)
    TextView numberOfRating;
    @BindView(R.id.custom_advice_rb_rating)
    RatingBar ratingBar;
    @BindView(R.id.custom_advice_iv_new)
    ImageView newAdvice;

    public AdviceAdapter(Context context, int resource, List<Advice> advices) {
        super(context, resource, advices);
        this.advices = advices;
        isFirstCall = true;
        states = new boolean[advices.size()];
    }

    public AdviceAdapter(Context context, int resource, List<Advice> advices, List<UserRatings> ratings) {
        super(context, resource, advices);
        this.advices = advices;
        this.ratings = ratings;
        isFirstCall = true;
        states = new boolean[advices.size()];
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final Advice currentAdvice = getItem(position);
        View view = convertView;

        if (view == null) {
            view = new ExpandAdviceView(getContext());
        }

        if (currentAdvice != null) {
            ButterKnife.bind(this, view);
            actionListener(view, currentAdvice);
            title.setText(currentAdvice.getTitle());
            desc.setText(currentAdvice.getDescription());

            if (isMainActivity) { //for MainActivity
                setMainView(currentAdvice, position);

            } else { //for SavedActivity
                newAdvice.setVisibility(View.GONE);
                numberOfRating.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);
                iconHeart.setImageResource(R.drawable.ic_favorite_black_24dp);
                Picasso.with(getContext()).load(currentAdvice.getImageUrl()).into(image);
            }
        }

        return view;
    }

    private void actionListener(View view, final Advice advice) {
        if (isMainActivity) {
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    firebaseAdapter = new FirebaseAdapter(getContext());
                    firebaseAdapter.isAdmin(new OnGetDataListener() {
                        @Override
                        public void onSuccess(DataSnapshot data) {
                            DialogInterface.OnClickListener dialog = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            updateAdviceFromServer(advice);
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            deleteAdviceFromServer(advice);
                                            break;
                                    }
                                }
                            };

                            showDialog(dialog, "Możliwe opcje:", "Edytuj", "Usuń");
                        }

                        @Override
                        public void onFailed(DatabaseError databaseError) {
                            Log.i(TAG, "onFailed: " + databaseError.getMessage());
                        }
                    });
                    return false;
                }
            });
        }

        iconHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                boolean isSaved = checkInLocalDatabase(advice);

                if (isSaved) {
                    //waring message
                    DialogInterface.OnClickListener dialog = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    deleteAdviceFromSaved(advice);
                                    Toast.makeText(getContext(), "Usunięto z zapisanych", Toast.LENGTH_SHORT).show();
                                    ((ImageView) view).setImageResource(R.drawable.ic_favorite_border_black_24dp);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    Toast.makeText(getContext(), "Anulowano", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                    };

                    showDialog(dialog, "Czy na pewno chcesz usunąć?", "Tak", "Nie");

                } else {
                    addAdviceToSaved(advice);
                    Toast.makeText(getContext(), "Dodano do zapisanych, możesz teraz przeglądać w trybie offline.", Toast.LENGTH_LONG).show();
                    ((ImageView) view).setImageResource(R.drawable.ic_favorite_black_24dp);
                }
            }
        });
    }

    private void setMainView(Advice advice, int position) {
        Picasso.with(getContext()).load(advice.getImageUrl()).into(image);
        numberOfRating.setText(String.valueOf(advice.getCountStar()));

        if(isFirstCall) { // for optimization
            isFirstCall = false;

            for(int i = 0; i< advices.size(); i++) {
                boolean isSaved = checkInLocalDatabase(getItem(i));
                states[i] = isSaved;
            }
        }

        if (states[position]) {
            iconHeart.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            iconHeart.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }
    }

    private void showDialog(DialogInterface.OnClickListener dialogClickListener, String message, String positive, String negative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message)
                .setPositiveButton(positive, dialogClickListener)
                .setNegativeButton(negative, dialogClickListener)
                .show();
    }

    private boolean checkInLocalDatabase(Advice currentAdvice) {
        String idCurrentUser = FirebaseAuth.getInstance().getUid();
        database = new DatabaseAdapter(getContext());

        Advice advice = database.getAdvice(currentAdvice.getId(), idCurrentUser);
        return advice != null;
    }

    private void addAdviceToSaved(Advice advice) {
        Log.i(TAG, "add: " + advice.toString());
        String idCurrentUser = FirebaseAuth.getInstance().getUid();
        database.addDataTableAdvice(advice, idCurrentUser);
    }

    private void deleteAdviceFromSaved(Advice advice) {
        Log.i(TAG, "delete: " + advice.toString());
        String idCurrentUser = FirebaseAuth.getInstance().getUid();
        database.deleteDataTableAdvice(advice.getId(), idCurrentUser);

        //update for offline advices in SavedActivity
        if (!isMainActivity) {
            List<Advice> newList = new ArrayList<>();

            for (Advice a : advices) {
                if (!a.getId().equals(advice.getId())) {
                    newList.add(a);
                }
            }

            clear();
            addAll(newList);
            advices = newList;
            search(searchText);

            if (advices.isEmpty()) {
                view.setDefaultText(View.VISIBLE);
            }
        }
    }

    //admin option
    private void updateAdviceFromServer(Advice advice) {
        Log.i(TAG, "updateAdviceFromServer: " + advice.toString());

        Intent intent = new Intent(getContext(), AdviceActivity.class);
        intent.putExtra("Advice", advice);
        getContext().startActivity(intent);
    }

    //admin option
    private void deleteAdviceFromServer(final Advice advice) {
        //waring message
        DialogInterface.OnClickListener dialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Log.i(TAG, "deleteAdviceFromServer: " + advice.toString());
                        firebaseAdapter.getStorage().child("images/" + advice.getId()).delete();
                        firebaseAdapter.deleteAdvice(advice.getId());
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        showDialog(dialog, "Czy na pewno chcesz usunąć?", "Tak", "Nie");
    }

    public void search(String text) {
        searchText = text;

        if (text != null && text.length() > 0) {
            List<Advice> searched = new ArrayList<>();
            for (Advice advice : advices) {
                if (advice.getTitle().toLowerCase().contains(text.toLowerCase()) ||
                        advice.getTag().toLowerCase().contains(text.toLowerCase())) {
                    searched.add(advice);
                }
            }
            clear();
            addAll(searched);
        } else {
            clear();
            addAll(advices);
        }
        notifyDataSetChanged();
    }

    public void setMainActivity(boolean visible) {
        isMainActivity = visible;
    }

    public void setSavedActivityView(SavedActivityPresenter.SavedActivityView view) {
        this.view = view;
    }

    public void setAdvices(List<Advice> list) {
        advices.clear();
        advices.addAll(list);
    }

    public List<Advice> getAdvices() {
        return advices;
    }

    public List<UserRatings> getRatings() {
        return ratings;
    }
}