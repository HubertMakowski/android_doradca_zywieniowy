package com.example.hpmak.android_doradca_zywieniowy.Model;

import java.io.Serializable;

public class Advice implements Serializable {

    private String id;
    private String imageUrl;
    private String title;
    private String description;
    private String category;
    private String tag;

    private int countStar;

    public Advice() {
    }

    public Advice(String id, String imageUrl, String title, String description, String category, String tag) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.title = title;
        this.description = description;
        this.category = category;
        this.tag = tag;
        this.countStar = 0;
    }

    @Override
    public String toString() {
        return "Advice{" +
                "id='" + id + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", tag='" + tag + '\'' +
                ", countStar=" + countStar +
                '}';
    }

    public String getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCountStar() {
        return countStar;
    }

    public void setCountStar(int countStar) {
        this.countStar = countStar;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
