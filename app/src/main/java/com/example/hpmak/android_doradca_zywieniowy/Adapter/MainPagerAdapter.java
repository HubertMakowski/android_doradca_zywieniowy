package com.example.hpmak.android_doradca_zywieniowy.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.hpmak.android_doradca_zywieniowy.View.Fragment.CuriosityFragment;
import com.example.hpmak.android_doradca_zywieniowy.View.Fragment.NutritionFragment;
import com.example.hpmak.android_doradca_zywieniowy.View.Fragment.SportFragment;

public class MainPagerAdapter extends FragmentStatePagerAdapter {

    public static final int SCREEN_CURIOSITY_FRAGMENT = 0;
    public static final int SCREEN_SPORT_FRAGMENT = 1;
    public static final int SCREEN_NUTRITION_FRAGMENT = 2;

    int mNumOfTabs;

    public MainPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case SCREEN_CURIOSITY_FRAGMENT:
                return new NutritionFragment();
            case SCREEN_SPORT_FRAGMENT:
                return new SportFragment();
            case SCREEN_NUTRITION_FRAGMENT:
                return new CuriosityFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
