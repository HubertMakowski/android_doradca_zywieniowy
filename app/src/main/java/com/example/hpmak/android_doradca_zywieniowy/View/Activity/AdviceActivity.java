package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.AdviceActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdviceActivity extends DrawerActivity implements AdviceActivityPresenter.AdviceActivityView, AdapterView.OnItemSelectedListener {

    private static final int PICK_IMAGE_REQUEST = 234;
    private static final String TAG = "AdviceActivity";
    private AdviceActivityPresenter presenter;

    @BindView(R.id.content_advice_tv_add)
    TextView textView;
    @BindView(R.id.content_advice_iv_image)
    ImageView image;
    @BindView(R.id.content_advice_et_title)
    EditText title;
    @BindView(R.id.content_advice_et_desc)
    EditText description;

    @BindView(R.id.content_advice_spinner_category)
    Spinner spinner;
    @BindView(R.id.content_advice_et_tag)
    EditText tag;

    private Uri uri = null; //sciezka do pliku na telefonie
    private String category;

    private Advice editingAdvice = null; //for editing

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_advice);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.category, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        presenter = new AdviceActivityPresenter(this);
        getIntentFromMainActivity();
    }

    private void getIntentFromMainActivity() {
        Intent intent = getIntent();
        editingAdvice = (Advice) intent.getSerializableExtra("Advice");

        if(editingAdvice != null) {
            textView.setVisibility(View.GONE);
            Picasso.with(this).load(editingAdvice.getImageUrl()).into(image);

            title.setText(editingAdvice.getTitle());
            description.setText(editingAdvice.getDescription());
            tag.setText(editingAdvice.getTag());
            category = editingAdvice.getCategory();
            //set spinner on category
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.advice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_advice_confirm) {
            if(editingAdvice != null) {
                presenter.replaceImage(uri, new Advice(editingAdvice.getId(), editingAdvice.getImageUrl(),
                        title.getText().toString(), description.getText().toString(),
                        category, tag.getText().toString())
                );
            } else {
                presenter.uploadImage(uri);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.rl_content_advice_image)
    public void onClickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Wybierz zdjęcie"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                uri = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                image.setImageBitmap(bitmap);
                textView.setVisibility(View.GONE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        TextView text = (TextView) view;
        category = (String) text.getText();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void goToMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public String getMyTitle() {
        return title.getText().toString();
    }

    @Override
    public String getDescription() {
        return description.getText().toString();
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String getTag() {
        return tag.getText().toString();
    }
}
