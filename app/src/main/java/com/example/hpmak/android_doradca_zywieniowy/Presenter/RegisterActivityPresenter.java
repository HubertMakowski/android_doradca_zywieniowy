package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.FirebaseAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Model.User;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.UUID;

import static android.content.Context.MODE_PRIVATE;

public class RegisterActivityPresenter {

    private RegisterActivityView view;

    public RegisterActivityPresenter(RegisterActivityView view) {
        this.view = view;
    }

    public interface RegisterActivityView {
        void printMessage(String toast, String debug);

        void goToProfilScreen();

        void showUserEmailError(int resId);

        void showUserPasswordError(int resId);

        void showUserRePasswordError(int resId);

        String getUserDisplayName();

        String getUserEmail();

        String getUserPassword();

        String getUserRePassword();
    }

    public boolean validateData() {
        String name = view.getUserDisplayName();
        String email = view.getUserEmail();
        String password = view.getUserPassword();
        String rePassword = view.getUserRePassword();

        if (name.isEmpty() || name.length() < 3) {
            view.showUserEmailError(R.string.user_name_error);
            return false;
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.showUserEmailError(R.string.user_email_error);
            return false;
        }
        if (password.isEmpty() || password.length() < 5) {
            view.showUserPasswordError(R.string.user_password_error_length);
            return false;
        }
        if (rePassword.isEmpty() || password.length() < 5) {
            view.showUserPasswordError(R.string.user_password_error_length);
            return false;
        }
        if (!password.equals(rePassword)) {
            view.showUserRePasswordError(R.string.user_password_error_similar);
            return false;
        }

        return true;
    }

    public void registerUser(String email, String password) {
        final ProgressDialog progressDialog = new ProgressDialog((Activity) view);
        progressDialog.setMessage("Rejestracja w toku...");
        progressDialog.show();

        FirebaseAdapter firebaseAdapter = new FirebaseAdapter((Activity) view);
        firebaseAdapter.registerUser(email, password, new com.example.hpmak.android_doradca_zywieniowy.Listener.OnCompleteListener() {
            @Override
            public void onComplete(String string) {
                view.printMessage(string, "registerUser()::success");
                progressDialog.dismiss();
                view.goToProfilScreen();
            }

            @Override
            public void onFailed(String string) {
                view.printMessage(string, "registerUser()::failed");
                progressDialog.dismiss();
            }
        });
    }
}
