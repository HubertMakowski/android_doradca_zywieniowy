package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.MainPagerAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Helper.NotificationHelper;
import com.example.hpmak.android_doradca_zywieniowy.Helper.NotificationReceiver;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetListListener;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Model.UserRatings;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.MainActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends DrawerActivity
        implements MainActivityPresenter.MainActivityView, NavigationView.OnNavigationItemSelectedListener,
                   GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "MainActivity";
    private MainActivityPresenter presenter;
    private SharedPreferences pref;

    private MainPagerAdapter mainPagerAdapter;
    private List<Advice> listNutrition;
    private List<Advice> listSport;
    private List<Advice> listCuriosity;
    private List<UserRatings> listUserRatings;

    @BindView(R.id.content_main_tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.content_main_view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        //first start up check
        pref = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        if(pref.getBoolean("firstStartup", true)) {
            pref.edit().putBoolean("firstStartup", false).apply();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            Log.i(TAG, "firstStartup");
        }

        tabLayout.addTab(tabLayout.newTab().setText("Odżywianie"));
        tabLayout.addTab(tabLayout.newTab().setText("Sport"));
        tabLayout.addTab(tabLayout.newTab().setText("Ciekawostki"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        presenter = new MainActivityPresenter(this);

        //TODO: test
        Intent notifyIntent = new Intent(this, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast
                (this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        System.out.println("TUTAJ MainActivity");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.set(Calendar.HOUR_OF_DAY, 7);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 5);
        calendar.add(Calendar.SECOND, 20);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,  calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Ładowanie danych z serwera");
        progressDialog.show();

        presenter.updateAdvices(new OnGetListListener() {
            @Override
            public void onComplete(Map<String, List<Advice>> mapListAdvice, List<UserRatings> ratings) {
                listNutrition = mapListAdvice.get("nutrition");
                listSport = mapListAdvice.get("sport");
                listCuriosity = mapListAdvice.get("curiosity");
                listUserRatings = ratings;

                mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
                viewPager.setAdapter(mainPagerAdapter);
                viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

                progressDialog.dismiss();
            }

            @Override
            public void onFailed(String error) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        presenter.isAdmin(new OnGetDataListener() {
            @Override
            public void onSuccess(DataSnapshot data) {
                menu.findItem(R.id.action_main_add_advice).setVisible(true);
                menu.findItem(R.id.action_main_notification).setVisible(true);
            }

            @Override
            public void onFailed(DatabaseError databaseError) {
                Log.i(TAG, "onFailed: " + databaseError.getMessage());
            }
        });

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_main_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                presenter.filterAdvices(lists, newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_main_add_advice) {
            startActivity(new Intent(this, AdviceActivity.class));
            finish();

        } else if (id == R.id.action_main_notification) {
            NotificationHelper notifi = new NotificationHelper(this);
            notifi.sendNotifi();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void goLoginScreen() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @Override
    public void updateUI(FirebaseUser user, UserRatings userRatings) {
//        likes.setText(userRatings.getRating());
    }

    @Override
    public void printMessage(String toast, String debug) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public List<Advice> getListNutrition() {
        return listNutrition;
    }

    public List<Advice> getListSport() {
        return listSport;
    }

    public List<Advice> getListCuriosity() {
        return listCuriosity;
    }

    public List<UserRatings> getListUserRatings() {
        return listUserRatings;
    }
}