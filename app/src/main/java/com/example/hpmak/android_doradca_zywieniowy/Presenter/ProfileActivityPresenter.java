package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.FirebaseAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnUploadDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.storage.UploadTask;

public class ProfileActivityPresenter {

    private static final String TAG = "ProfileActivity";

    private ProfileActivityView view;
    private FirebaseAdapter firebaseAdapter;

    private User user;
    private String idCurrentUser;

    public ProfileActivityPresenter(ProfileActivityView view) {
        this.view = view;
        idCurrentUser = FirebaseAuth.getInstance().getUid();
        firebaseAdapter = new FirebaseAdapter((Activity)view);
    }

    public interface ProfileActivityView {
        void goMainScreen();
        void setProfile(User user);
        void setIvProfile(Uri uri);
        ImageView getImage();
        String getEmail();
        String getDisplayName();
        String getBirthdate();
        String getGender();
        String getTimeNotification();
        String getTag();
    }

    public void getData() {
        firebaseAdapter.setProfileImage(view, FirebaseAuth.getInstance().getUid());

        firebaseAdapter.getUser(idCurrentUser, new OnGetDataListener() {

            @Override
            public void onSuccess(DataSnapshot data) {
                user = data.getValue(User.class);

                if(user != null) {
                    Log.i("OnGetDataListener", "onSuccess: " + user.toString());
                    view.setProfile(user);
                }
            }

            @Override
            public void onFailed(DatabaseError databaseError) {
                Log.d(TAG, "onFailed: " + databaseError.getMessage());
            }
        });
    }

    public void saveData() {
        user.setEmail(view.getEmail());
        user.setDisplayName(view.getDisplayName());
        user.setGender(view.getGender());
        user.setBirthDate(view.getBirthdate());
        user.setTimeNotification(view.getTimeNotification());
        user.setTag(view.getTag());

        Log.i(TAG, "saveData: " + user.toString());
        firebaseAdapter.updateUser(idCurrentUser, user);
        if(firebaseAdapter.getUri() == null) {
            view.goMainScreen();
        }

        firebaseAdapter.uploadData("profile/" + idCurrentUser, new OnUploadDataListener() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.i(TAG, "onSuccess: bytes transferred: " + taskSnapshot.getBytesTransferred());
                view.goMainScreen();
            }

            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.i(TAG, "onFailure: " + exception.getMessage());
            }
        });
    }

    public FirebaseAdapter getFirebaseAdapter() {
        return firebaseAdapter;
    }
}
