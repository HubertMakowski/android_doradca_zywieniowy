package com.example.hpmak.android_doradca_zywieniowy.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.hpmak.android_doradca_zywieniowy.Helper.DatabaseHelper;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAdapter implements IDatabaseAdapter {

    private Context context;
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;

    public DatabaseAdapter(Context context) {
        this.context = context;
    }

    private IDatabaseAdapter openDatabase() {
        try {
            dbHelper = new DatabaseHelper(context);
            database = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    private boolean closeDatabase() {
        if (dbHelper != null) {
            dbHelper.close();
            return true;
        }
        return false;
    }

    @Override
    public List<Advice> getAllDataTableAdvice() {
        openDatabase();

        Cursor cursor = database.rawQuery("Select * From " + DatabaseHelper.ADVICE, null);

        if (cursor.getCount() == 0) return null;

        List<Advice> list = new ArrayList<>();
        list.clear();
        while (cursor.moveToNext()) {
            list.add(new Advice(cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6)));
        }

        cursor.close();
        closeDatabase();
        return list;
    }

    @Override
    public List<Advice> getDataTableAdvice(String idUser) {
        openDatabase();
        Cursor cursor;

        if(idUser == null) {
            cursor = database.rawQuery("Select * From " + DatabaseHelper.ADVICE + " Where USER_UID is null", null);
        }
        else {
            cursor = database.rawQuery("Select * From " + DatabaseHelper.ADVICE + " Where USER_UID = \"" + idUser + "\"", null);
        }

        if (cursor.getCount() == 0) return null;

        List<Advice> list = new ArrayList<>();
        list.clear();
        while (cursor.moveToNext()) {
            list.add(new Advice(cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6)));
        }

        cursor.close();
        closeDatabase();
        return list;
    }

    @Override
    public Advice getAdvice(String id, String idUser) {
        openDatabase();
        Cursor cursor;

        if(idUser == null) {
            cursor = database.rawQuery("Select * From " + DatabaseHelper.ADVICE + " Where USER_UID is null AND ADVICE_ID = " + id, null);
        }
        else {
            cursor = database.rawQuery("Select * From " + DatabaseHelper.ADVICE + " Where USER_UID = \"" + idUser + "\" AND ADVICE_ID = " + id, null);
        }

        if (cursor.getCount() == 0) return null;

        Advice advice = null;
        while (cursor.moveToNext()) {
            advice = new Advice(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
        }

        cursor.close();
        closeDatabase();
        return advice;
    }

    @Override
    public boolean addDataTableAdvice(Advice advice, String idUser) {
        openDatabase();

        ContentValues values = new ContentValues();

        values.put("advice_id", advice.getId());
        values.put("image_url", advice.getImageUrl());
        values.put("title", advice.getTitle());
        values.put("description", advice.getDescription());
        values.put("category", advice.getCategory());
        values.put("tag", advice.getTag());
        values.put("user_uid", idUser);

        long result = database.insert(DatabaseHelper.ADVICE, null, values);
        closeDatabase();
        return (result == -1);
    }

    @Override
    public boolean updateDataTableAdvice(Advice advice, String idUser) {
        openDatabase();

        ContentValues values = new ContentValues();
        values.put("advice_id", advice.getId());
        values.put("image_url", advice.getImageUrl());
        values.put("title", advice.getTitle());
        values.put("description", advice.getDescription());
        values.put("category", advice.getCategory());
        values.put("tag", advice.getTag());
        values.put("user_uid", idUser);

        long result = database.update(DatabaseHelper.ADVICE, values, "USER_UID = \""+idUser+"\" AND ADVICE_ID = ?", new String[]{advice.getId()});
        closeDatabase();
        return (result == -1);
    }

    @Override
    public Integer deleteDataTableAdvice(String id, String idUser) {
        openDatabase();
        int state;

        if(idUser == null) {
            state = database.delete(DatabaseHelper.ADVICE, "USER_UID is null AND ADVICE_ID = ?", new String[]{id});
        } else {
            state = database.delete(DatabaseHelper.ADVICE, "USER_UID = \""+idUser+"\" AND ADVICE_ID = ?", new String[]{id});
        }

        closeDatabase();
        return state;
    }

    @Override
    public void deleteAllData(String tablename) {
        openDatabase();
        database.execSQL("Delete From " + tablename + ';');
        closeDatabase();
    }

    @Override
    public void deleteTable(String tablename) {
        openDatabase();
        database.execSQL("drop table if exists " + tablename + ';');
        closeDatabase();
    }

    @Override
    public void insertToDB(String sql) {
        openDatabase();
        database.execSQL(sql);
        closeDatabase();
    }
}
