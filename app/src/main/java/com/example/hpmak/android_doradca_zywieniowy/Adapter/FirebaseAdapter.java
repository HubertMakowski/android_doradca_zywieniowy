package com.example.hpmak.android_doradca_zywieniowy.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnUploadDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Model.UserRatings;
import com.example.hpmak.android_doradca_zywieniowy.Model.User;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.ProfileActivityPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class FirebaseAdapter {

    public static final String TAG = "FirebaseAdapter";

    public static final String USERS = "Users";
    public static final String ADVICES = "Advices";
    public static final String USERS_RATINGS = "UsersRatings";

    private Context context;
    private DatabaseReference database;
    private StorageReference storage;

    private String downloadUri;
    private Uri uri;

    public FirebaseAdapter(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        FirebaseApp.initializeApp(context);
        database = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance().getReference();
    }

    //////////////////////////////////////////////////////////ADD///////////////////////////////////////////////////////////////////////////////////////////
    public void addUser(User user) {
        database.child(USERS).child(user.getId()).setValue(user);
    }

    public void addAdvice(Advice advice) {
        database.child(ADVICES).child(advice.getId()).setValue(advice);
    }

    //////////////////////////////////////////////////////////GET///////////////////////////////////////////////////////////////////////////////////////////
    public void getUser(String id, final OnGetDataListener listener) {
        database.child(USERS).child(id).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onSuccess(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError);
            }
        });
    }

    //////////////////////////////////////////////////////////UPDATE///////////////////////////////////////////////////////////////////////////////////////////
    public void updateUser(String id, User newUser) {
        database.child(USERS).child(id).setValue(newUser);
    }

    public void updateAdvice(Advice advice) {
        database.child(ADVICES).child(advice.getId()).setValue(advice);
    }

    //////////////////////////////////////////////////////////DELETE///////////////////////////////////////////////////////////////////////////////////////////
    public void deleteUser(String id) {
        database.child(USERS).child(id).removeValue();
    }

    public void deleteAdvice(String id) {
        database.child(ADVICES).child(id).removeValue();
    }

    public void registerUser(final String email, final String password, final com.example.hpmak.android_doradca_zywieniowy.Listener.OnCompleteListener listener) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Create currentUser in the database
                            User user = new User(mAuth.getUid(), email, password, "user");
                            addUser(user);
                            listener.onComplete("Rejestracja udana");

                        } else {
                            listener.onFailed("Rejestracja nie udana, istnieje już konto z podanym adresem e-mail");
                        }
                    }
                });
    }

    public void isAdmin(final OnGetDataListener listener) {
        final String idUser = FirebaseAuth.getInstance().getUid();

        if(idUser != null) {
            database.child(FirebaseAdapter.USERS).child(idUser).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);

                    if(user != null && user.getRole().equals("admin")) {
                        Log.i("OnGetDataListener", "Role: " + user.getRole());
                        listener.onSuccess(dataSnapshot);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    listener.onFailed(databaseError);
                }
            });
        }
    }

    public void uploadData(String path, final OnUploadDataListener listener) {
        if(uri != null) {
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Przesyłanie zdjęcia");
            progressDialog.show();

            StorageReference riversRef = storage.child(path);

            riversRef.putFile(uri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            listener.onSuccess(taskSnapshot);
                            progressDialog.dismiss();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG).show();
                            listener.onFailure(exception);
                            progressDialog.dismiss();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage(((int)progress) + "% Przesłano...");
                        }
                    });
        }
    }

    public void setProfileImage(final ProfileActivityPresenter.ProfileActivityView view, String idUser) {
        StorageReference storageRef = storage.child("/profile");

        storageRef.child(idUser).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.i(TAG, "onSuccess: " + uri.toString());
                view.setIvProfile(uri);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.i(TAG, "onFailure: " + exception.getMessage());
            }
        });
    }

    public String getImageExt(Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public DatabaseReference getDatabase() {
        return database;
    }

    public StorageReference getStorage() {
        return storage;
    }

    public String getDownloadUri() {
        return downloadUri;
    }

    public void setDownloadUri(String downloadUri) {
        this.downloadUri = downloadUri;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
