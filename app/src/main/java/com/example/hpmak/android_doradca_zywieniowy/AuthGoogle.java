package com.example.hpmak.android_doradca_zywieniowy;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.example.hpmak.android_doradca_zywieniowy.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class AuthGoogle {

    private GoogleSignInOptions signInOptions;
    private GoogleApiClient apiClient;

    public AuthGoogle(Context context) {
        signInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        apiClient = new GoogleApiClient.Builder(context)
                .enableAutoManage((FragmentActivity) context, (GoogleApiClient.OnConnectionFailedListener)context)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build();
    }

    public GoogleSignInOptions getSignInOptions() {
        return signInOptions;
    }

    public void setSignInOptions(GoogleSignInOptions signInOptions) {
        this.signInOptions = signInOptions;
    }

    public GoogleApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(GoogleApiClient apiClient) {
        this.apiClient = apiClient;
    }
}
