package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.app.SearchManager;
import android.content.Context;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hpmak.android_doradca_zywieniowy.Helper.NotificationHelper;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.SavedActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SavedActivity extends DrawerActivity implements SavedActivityPresenter.SavedActivityView, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "SavedActivity";
    private SavedActivityPresenter presenter;

    private List<Advice> advices;

    @BindView(R.id.content_saved_tv_default)
    TextView defaultText;
    @BindView(R.id.content_saved_lv_saved)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_saved);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        presenter = new SavedActivityPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.updateAdvices(listView);
        listView.setSelectionFromTop(3, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_main_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.filterAdvices(listView, newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_main_add_advice) {
            return true;
        } else if (id == R.id.action_main_notification) {
//            NotificationHelper notifi = new NotificationHelper(this);
//            notifi.sendNotifi();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setDefaultText(int view) {
        defaultText.setVisibility(view);
    }
}
