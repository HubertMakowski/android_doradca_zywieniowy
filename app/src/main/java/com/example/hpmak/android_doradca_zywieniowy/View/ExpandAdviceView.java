package com.example.hpmak.android_doradca_zywieniowy.View;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.example.hpmak.android_doradca_zywieniowy.R;

public class ExpandAdviceView extends ExpandBaseView {

    public ExpandAdviceView(Context context) {
        super(context);
        init();
    }

    public ExpandAdviceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExpandAdviceView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.custom_advice, this);
        this.layout = findViewById(R.id.custom_advice_ll_clickable);
        this.animateLayout = findViewById(R.id.custom_advice_ll_desc);
        animate(layout, animateLayout);
    }
}
