package com.example.hpmak.android_doradca_zywieniowy.Listener;

public interface OnCompleteListener {
    void onComplete(String string);
    void onFailed(String string);
}
