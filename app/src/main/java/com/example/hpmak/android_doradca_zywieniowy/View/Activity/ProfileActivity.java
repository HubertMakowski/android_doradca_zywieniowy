package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.hpmak.android_doradca_zywieniowy.Model.User;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.ProfileActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.example.hpmak.android_doradca_zywieniowy.SharedPrefUtil;
import com.example.hpmak.android_doradca_zywieniowy.View.Fragment.DatePickerFragment;
import com.example.hpmak.android_doradca_zywieniowy.View.Fragment.TimePickerFragment;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends DrawerActivity implements ProfileActivityPresenter.ProfileActivityView, NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener, DatePickerFragment.DatePickerFragmentListener, TimePickerFragment.TimePickerFragmentListener {

    private static final int PICK_IMAGE_REQUEST = 234;
    private static final String TAG = "ProfileActivity";
    private ProfileActivityPresenter presenter;

    @BindView(R.id.content_profile_iv_image)
    ImageView ivProfile;
    @BindView(R.id.content_profile_tv_email)
    TextView tvEmail;
    @BindView(R.id.content_profile_tv_name)
    TextView tvDisplayName;
    @BindView(R.id.content_profile_tv_birthdate)
    TextView tvDateOfBirth;
    @BindView(R.id.content_profile_tv_time)
    TextView tvTimeNotification;
    @BindView(R.id.content_profile_spinner_gender)
    Spinner spinnerGender;
    @BindView(R.id.content_profile_et_tag)
    EditText etTag;

    @BindView(R.id.content_profile_ll_advices)
    LinearLayout llAdvices;
    @BindView(R.id.content_profile_ll_advanced_advices)
    LinearLayout llAdvancedAdvices;
    @BindView(R.id.content_profile_ib_time)
    ImageButton ibTimeNotification;

    @BindView(R.id.content_profile_cb_notifications)
    CheckBox cbNotification;
    @BindView(R.id.content_profile_cb_nutrition)
    CheckBox cbNutrition;
    @BindView(R.id.content_profile_cb_sport)
    CheckBox cbSport;
    @BindView(R.id.content_profile_cb_curiosities)
    CheckBox cbCuriosities;
    @BindView(R.id.content_profile_cb_advanced)
    CheckBox cbAdvancedNotification;

    private ArrayAdapter adapter;
    private ProgressDialog progressDialog;
    private SharedPrefUtil preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Pobieranie danych...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        adapter = ArrayAdapter.createFromResource(this, R.array.gender, android.R.layout.simple_spinner_item);
        spinnerGender.setAdapter(adapter);
        spinnerGender.setOnItemSelectedListener(this);

        preferences = new SharedPrefUtil(this);
        if(preferences.getBoolean("isChanged")) {
            cbNotification.setChecked(preferences.getBoolean("cbNotification"));
            cbNutrition.setChecked(preferences.getBoolean("cbNutrition"));
            cbSport.setChecked(preferences.getBoolean("cbSport"));
            cbCuriosities.setChecked(preferences.getBoolean("cbCuriosities"));
            cbAdvancedNotification.setChecked(preferences.getBoolean("cbAdvancedNotification"));
            changeAdviceLayout();
            changeAdvancedAdviceLayout();
        }

        presenter = new ProfileActivityPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile_save:
                presenter.saveData();
                saveCheckboxState();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveCheckboxState() {
        preferences.saveBoolean("isChanged", true);
        preferences.saveBoolean("cbNotification", cbNotification.isChecked());
        preferences.saveBoolean("cbNutrition", cbNutrition.isChecked());
        preferences.saveBoolean("cbSport", cbSport.isChecked());
        preferences.saveBoolean("cbCuriosities", cbCuriosities.isChecked());
        preferences.saveBoolean("cbAdvancedNotification", cbAdvancedNotification.isChecked());
    }

    @OnClick(R.id.content_profile_iv_image)
    void setProfileImage() {
        showFileChooser();
    }

    @OnClick(R.id.content_profile_ll_birthdate)
    void showDatePickerFragment() {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "DatePicker");
    }

    @OnClick(R.id.content_profile_ib_time)
    void showTimePickerFragment() {
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.show(getSupportFragmentManager(), "TimePicker");
    }

    @OnClick(R.id.content_profile_cb_notifications)
    void changeAdviceLayout() {
        if(cbNotification.isChecked()) {
            llAdvices.setBackgroundColor(getResources().getColor(R.color.white));
            ibTimeNotification.setBackgroundColor(getResources().getColor(R.color.white));
            ibTimeNotification.setClickable(true);
            cbAdvancedNotification.setClickable(true);
            cbNutrition.setClickable(true);
            cbSport.setClickable(true);
            cbCuriosities.setClickable(true);
        }
        else {
            llAdvices.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            llAdvancedAdvices.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            ibTimeNotification.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            ibTimeNotification.setClickable(false);
            cbAdvancedNotification.setClickable(false);
            cbNutrition.setClickable(false);
            cbSport.setClickable(false);
            cbCuriosities.setClickable(false);
            cbAdvancedNotification.setChecked(false);
            cbNutrition.setChecked(false);
            cbSport.setChecked(false);
            cbCuriosities.setChecked(false);
            etTag.setText("");
            etTag.setClickable(false);
            etTag.setCursorVisible(false);
            etTag.setFocusable(false);
            etTag.setFocusableInTouchMode(false);
        }
    }

    @OnClick(R.id.content_profile_cb_advanced)
    void changeAdvancedAdviceLayout() {
        if(cbAdvancedNotification.isChecked()) {
            llAdvancedAdvices.setBackgroundColor(getResources().getColor(R.color.white));
            etTag.setClickable(true);
            etTag.setCursorVisible(true);
            etTag.setFocusable(true);
            etTag.setFocusableInTouchMode(true);
        }
        else {
            llAdvancedAdvices.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            etTag.setText("");
            etTag.setClickable(false);
            etTag.setCursorVisible(false);
            etTag.setFocusable(false);
            etTag.setFocusableInTouchMode(false);
        }
    }

    public void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Wybierz zdjęcie profilowe"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            presenter.getFirebaseAdapter().setUri(data.getData());

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                ivProfile.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void goMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void setProfile(User user) {
        tvDisplayName.setText(user.getDisplayName());
        tvEmail.setText(user.getEmail());

        int pos = adapter.getPosition(user.getGender());
        spinnerGender.setSelection(pos);

        tvDateOfBirth.setText(user.getBirthDate());
        tvTimeNotification.setText(user.getTimeNotification());

        Log.i(TAG, "setProfile: " + user.getTag());
//        String tag = parsingTag(StringUtils.split(currentUser.getTag()," ,."));
        etTag.setText(user.getTag());

        //set the cursor to the right
        if (etTag.getText().length() > 0 ) {
            etTag.setSelection(etTag.getText().length());
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        }, 1000);
    }

    @Override
    public void setIvProfile(Uri uri) {
        Picasso.with(this).load(uri).into(ivProfile);
    }

    @Override
    public ImageView getImage() {
        return ivProfile;
    }

    @Override
    public String getEmail() {
        return tvEmail.getText().toString();
    }

    @Override
    public String getDisplayName() {
        return tvDisplayName.getText().toString();
    }

    @Override
    public String getBirthdate() {
        return tvDateOfBirth.getText().toString();
    }

    @Override
    public String getGender() {
        return spinnerGender.getSelectedItem().toString();
    }

    @Override
    public String getTimeNotification() {
        return tvTimeNotification.getText().toString();
    }

    @Override
    public String getTag() {
        String tag = "";

        if(cbNotification.isChecked()) {
            if(cbAdvancedNotification.isChecked()) {
                tag = etTag.getText().toString();
            }
        }

        return tag;
    }

    @Override
    public void setTvDateOfBirth(String date) {
        tvDateOfBirth.setText(date);
    }

    @Override
    public void setTimeNotification(String time) {
        tvTimeNotification.setText(time);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
