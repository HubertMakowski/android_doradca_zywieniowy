package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.AdviceAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Adapter.DatabaseAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class SavedActivityPresenter {

    private String TAG = "SavedActivityPresenter";

    private SavedActivityView view;
    private DatabaseAdapter database;

    private List<Advice> advices;
    private List<Advice> advicesCopy;
    private String idCurrentUser;

    public SavedActivityPresenter(SavedActivityView view) {
        this.view = view;
        database = new DatabaseAdapter((Activity)view);
        advices = new ArrayList<>();
    }

    public interface SavedActivityView {
        void setDefaultText(int view);
    }

    public void updateAdvices(ListView listView) {
        idCurrentUser = FirebaseAuth.getInstance().getUid();
        advices = database.getDataTableAdvice(idCurrentUser);
        advicesCopy = advices;

        if(advices != null) {
            view.setDefaultText(View.GONE);
            AdviceAdapter adapter = new AdviceAdapter((Context)view, 0, advices);
            adapter.setSavedActivityView(view);
            adapter.setMainActivity(false);
            listView.setAdapter(adapter);
//            ViewGroup.LayoutParams params = listView.getLayoutParams();
//            params.height = textView.getLayoutParams().height;
//            listView.setLayoutParams(params);
        }
        else {
            view.setDefaultText(View.VISIBLE);
        }
    }

    public void filterAdvices(ListView listView, String newText) {
        advices = database.getDataTableAdvice(idCurrentUser);

        AdviceAdapter adapter = new AdviceAdapter((Context) view, 0, advices);
        adapter.setSavedActivityView(view);
        adapter.setMainActivity(false);
        advices = new ArrayList<>(advicesCopy);

        if(newText != null) {
            adapter.search(newText);
        }

        listView.setAdapter(adapter);
    }
}
