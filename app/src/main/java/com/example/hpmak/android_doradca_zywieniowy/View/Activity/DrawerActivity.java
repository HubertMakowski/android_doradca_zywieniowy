package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hpmak.android_doradca_zywieniowy.R;
import com.facebook.stetho.Stetho;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        ImageView userImage = header.findViewById(R.id.nav_header_drawer_iv_user);
        TextView userName = header.findViewById(R.id.nav_header_drawer_tv_name);
        TextView userEmail = header.findViewById(R.id.nav_header_drawer_tv_email);

        //set Login or Logout UI
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
        } else {
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_tips) {
            startActivity(new Intent(this, MainActivity.class));
            finish();

        } else if (id == R.id.nav_storage) {
            startActivity(new Intent(this, SavedActivity.class));
            finish();

        } else if (id == R.id.nav_account) {
            if (currentUser != null) {
                startActivity(new Intent(this, ProfileActivity.class));
                finish();
            } else {
                Toast.makeText(this, "Musisz być zalogowany.", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_help) {
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"h.p.makowski@gmail.com"});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Pomoc z aplikacji 'Doradca żywieniowy'");
            startActivity(Intent.createChooser(emailIntent, "Wyślij email"));
            finish();

        } else if (id == R.id.nav_about) {

        } else if (id == R.id.nav_logout) {
            if (currentUser != null) {
                FirebaseAuth.getInstance().signOut();
            }

            //WYLOGUJ / ZALOGUJ
//            Auth.GoogleSignInApi.signOut(authGoogle.getApiClient()).setResultCallback(new ResultCallback<Status>() {
//                @Override
//                public void onResult(@NonNull Status status) {
//                    if (status.isSuccess()) {
//                        goLoginScreen();
//                    } else {
//                        Log.i(TAG, "onResult: status failed");
//                    }
//                }
//            });

            startActivity(new Intent(this, LoginActivity.class));
            finish();

        } else if (id == R.id.nav_login) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
