package com.example.hpmak.android_doradca_zywieniowy.Helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.example.hpmak.android_doradca_zywieniowy.R;

public class NotificationHelper {

    private Context context;

    public NotificationHelper(Context context) {
        this.context = context;
    }

    public void sendNotifi() {
        initChannels();
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = 1;
        String CHANNEL_ID = "default";

        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        Notification notification =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setContentTitle("Porada dnia")
                        .setContentText("Przykładowy tekst, kliknij, aby przejść do pełnej zawartości...")
                        .setSmallIcon(R.drawable.ic_notifications_white_36dp)
                        .build();

        // Issue the notification.
        mNotificationManager.notify(notificationID, notification);
    }

    private void initChannels() {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("default",
                "Channel name",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        notificationManager.createNotificationChannel(channel);
    }
}
