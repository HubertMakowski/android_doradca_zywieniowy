package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hpmak.android_doradca_zywieniowy.AuthGoogle;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.LoginActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginActivityPresenter.LoginActivityView, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static String TAG = "LoginActivity";
    private static final int SIGN_IN_CODE = 777;

    private LoginActivityPresenter presenter;
    private CallbackManager mCallbackManager;
    private AuthGoogle authGoogle;

    @BindView(R.id.activity_login_et_email)
    EditText loginEmail;
    @BindView(R.id.activity_login_et_password)
    EditText loginPassword;
    @BindView(R.id.activity_login_btn_email)
    Button btnLoginEmail;
    @BindView(R.id.activity_login_tv_register_link)
    TextView registerLink;
    @BindView(R.id.activity_login_btn_facebook)
    Button btnLoginFacebook;
    @BindView(R.id.activity_login_btn_google)
    SignInButton btnLoginGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        presenter = new LoginActivityPresenter(this);
        mCallbackManager = CallbackManager.Factory.create();
        authGoogle = new AuthGoogle(this); //TODO: poprawione
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = presenter.getAuth().getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            Log.d(TAG, "onActivityResult: Facebook exception: " + e.getMessage());
        }

        try {
            if (requestCode == SIGN_IN_CODE) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    presenter.firebaseAuthWithGoogle(account);
                } catch (ApiException e) {
                    // Google Sign In failed, update UI appropriately
                    Log.d(TAG, "Google sign in failed", e);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "onActivityResult: Google exception: " + e.getMessage());
        }
    }

    @OnClick(R.id.activity_login_btn_email)
    void signFromEmail() {
        boolean isCorrect = presenter.validateData();

        if (isCorrect) {
            presenter.firebaseAuthWithEmail(getUserEmail(), getUserPassword());
        }
    }

    @OnClick(R.id.activity_login_tv_register_link)
    void goToRegisterScreen() {
        startActivity(new Intent(this, RegisterActivity.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }

    @OnClick(R.id.activity_login_btn_facebook)
    void signFromFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                presenter.handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }

    @OnClick(R.id.activity_login_btn_google)
    void signFromGoogle() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(authGoogle.getApiClient());
        startActivityForResult(intent, SIGN_IN_CODE);
    }

    @OnClick(R.id.tv_activity_login_skip)
    public void goToMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void printMessage(String toast, String debug) {
        Toast.makeText(LoginActivity.this, toast, Toast.LENGTH_SHORT).show();
        Log.d(TAG, debug);
    }

    @Override
    public void updateUI(FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void showUserEmailError(int resId) {
        loginEmail.setError(getString(resId));
    }

    @Override
    public void showUserPasswordError(int resId) {
        loginPassword.setError(getString(resId));
    }

    @Override
    public String getUserEmail() {
        return loginEmail.getText().toString().trim();
    }

    @Override
    public String getUserPassword() {
        return loginPassword.getText().toString().trim();
    }

    //TODO; przeniesc do AuthGoogle;
    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            presenter.firebaseAuthWithGoogle(account);
            startActivity(new Intent(this, MainActivity.class));
        } else {
            updateUI(null);
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(authGoogle.getApiClient()).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUI(null);
            }
        });
    }
}
