package com.example.hpmak.android_doradca_zywieniowy.View.Fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.AdviceAdapter;
import com.example.hpmak.android_doradca_zywieniowy.R;
import com.example.hpmak.android_doradca_zywieniowy.View.Activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CuriosityFragment extends Fragment {

    @BindView(R.id.content_main_curiosity_lv)
    ListView listView;
    @BindView(R.id.content_main_curiosity_tv_number)
    TextView numberOfNotifications;
    @BindView(R.id.content_main_curiosity_iv_notification)
    ImageView notification;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.content_main_curiosity, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            AdviceAdapter adapter = new AdviceAdapter(getContext(), 0, ((MainActivity) getActivity()).getListCuriosity());
            listView.setAdapter(adapter);
        }
    }
}
