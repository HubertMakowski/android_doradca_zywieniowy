package com.example.hpmak.android_doradca_zywieniowy.Helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BroadcastReceiver {

    public NotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("TUTAJ NotificationReceiver");
        Intent service = new Intent(context, NotificationIntentService.class);
        context.startService(service);
    }
}
