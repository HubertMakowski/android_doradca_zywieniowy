package com.example.hpmak.android_doradca_zywieniowy.Listener;

import android.support.annotation.NonNull;
import com.google.firebase.storage.UploadTask;

public interface OnUploadDataListener {
    void onSuccess(UploadTask.TaskSnapshot taskSnapshot);
    void onFailure(@NonNull Exception exception);
}
