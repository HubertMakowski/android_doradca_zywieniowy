package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import android.content.Context;
import android.util.Log;
import android.widget.ListView;

import com.example.hpmak.android_doradca_zywieniowy.Adapter.AdviceAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Adapter.FirebaseAdapter;
import com.example.hpmak.android_doradca_zywieniowy.Helper.DatabaseHelper;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetDataListener;
import com.example.hpmak.android_doradca_zywieniowy.Listener.OnGetListListener;
import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import com.example.hpmak.android_doradca_zywieniowy.Model.UserRatings;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivityPresenter {

    private String TAG = "MainActivityPresenter";
    private MainActivityView view;
    private FirebaseAdapter firebase;

    private List<Advice> advicesNutrition;
    private List<Advice> advicesSport;
    private List<Advice> advicesCuriosity;

    //for dynamic refresh in the search
    private List<Advice> advicesNutritionCopy;
    private List<Advice> advicesSportCopy;
    private List<Advice> advicesCuriosityCopy;

    private List<UserRatings> listUserRatings;

    public MainActivityPresenter(MainActivityView view) {
        this.view = view;
        firebase = new FirebaseAdapter((Context) view);
        new DatabaseHelper((Context) view); //local database

        advicesNutrition = new ArrayList<>();
        advicesSport = new ArrayList<>();
        advicesCuriosity = new ArrayList<>();

        advicesNutritionCopy = new ArrayList<>();
        advicesSportCopy = new ArrayList<>();
        advicesCuriosityCopy = new ArrayList<>();

        listUserRatings = new ArrayList<>();
    }

    public interface MainActivityView {
        void updateUI(FirebaseUser user, UserRatings userRatings);

        void printMessage(String toast, String debug);

        void goLoginScreen();
    }

    public void updateAdvices(final OnGetListListener listener) {
        firebase.getDatabase().child(FirebaseAdapter.ADVICES).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                advicesNutrition.clear();
                advicesSport.clear();
                advicesCuriosity.clear();
                advicesNutritionCopy.clear();
                advicesSportCopy.clear();
                advicesCuriosityCopy.clear();
                listUserRatings.clear();

                for (DataSnapshot obj : dataSnapshot.getChildren()) {
                    Advice advice = obj.getValue(Advice.class);

                    if(advice != null) {
                        //get Count user's
//                        if(dataSnapshot.child(advice.getId()).hasChild(FirebaseAdapter.USERS_RATINGS)) {
//                            long countStar = dataSnapshot.child(advice.getId()).child(FirebaseAdapter.USERS_RATINGS).getChildrenCount();
//                            advice.setCountStar((int)countStar);
//                        }

                        switch (advice.getCategory()) {
                            case "odżywianie":
                                advicesNutrition.add(advice);
                                advicesNutritionCopy.add(advice);
                                break;
                            case "sport":
                                advicesSport.add(advice);
                                advicesSportCopy.add(advice);
                                break;
                            case "ciekawostki":
                                advicesCuriosity.add(advice);
                                advicesCuriosityCopy.add(advice);
                                break;
                            default:
                                Log.i(TAG, "onDataChange: Category not found.");
                        }

                        //for login user
                        String idUser = FirebaseAuth.getInstance().getUid();
                        if(idUser != null && dataSnapshot.child(advice.getId()).hasChild(FirebaseAdapter.USERS_RATINGS)) {
                            if(dataSnapshot.child(advice.getId()).child(FirebaseAdapter.USERS_RATINGS).hasChild(idUser)) {
                                Boolean isRead = ((Boolean) dataSnapshot.child(advice.getId()).child(FirebaseAdapter.USERS_RATINGS).child(idUser).child("isRead").getValue());
                                long rating = (long) dataSnapshot.child(advice.getId()).child(FirebaseAdapter.USERS_RATINGS).child(idUser).child("rating").getValue();

                                if(isRead != null) {
                                    UserRatings userRatings = new UserRatings(advice.getId(), (int)rating, isRead);
                                    Log.i(TAG, "onDataChange: listUserRatings" + userRatings.toString());
                                    listUserRatings.add(userRatings);
                                }
                            }
                        }
                    }
                }

                Map<String, List<Advice>> mapCategory = new HashMap<>();
                mapCategory.put("nutrition", advicesNutrition);
                mapCategory.put("sport", advicesSport);
                mapCategory.put("curiosity", advicesCuriosity);
                listener.onComplete(mapCategory, listUserRatings);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.onFailed(databaseError.getMessage());
            }
        });
    }

    public void isAdmin(final OnGetDataListener listener) {
        firebase.isAdmin(listener);
    }

    public void filterAdvices(final List<ListView> lists, String newText) {
        AdviceAdapter adapterNutrition = new AdviceAdapter((Context) view, 0, advicesNutrition);
        AdviceAdapter adapterSport = new AdviceAdapter((Context) view, 0, advicesSport);
        AdviceAdapter adapterCuriosity = new AdviceAdapter((Context) view, 0, advicesCuriosity);
        advicesNutrition = new ArrayList<>(advicesNutritionCopy);
        advicesSport = new ArrayList<>(advicesSportCopy);
        advicesCuriosity = new ArrayList<>(advicesCuriosityCopy);

        if(newText != null) {
            adapterNutrition.search(newText);
            adapterSport.search(newText);
            adapterCuriosity.search(newText);
        }

        lists.get(0).setAdapter(adapterNutrition);
        lists.get(1).setAdapter(adapterSport);
        lists.get(2).setAdapter(adapterCuriosity);
    }
}