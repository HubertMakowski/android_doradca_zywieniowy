package com.example.hpmak.android_doradca_zywieniowy.Helper;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

public class NotificationIntentService extends IntentService {

    public NotificationIntentService() {
        super("NotificationIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        System.out.println("TUTAJ NotificationIntentService");
        NotificationHelper notification = new NotificationHelper(this);
        notification.sendNotifi();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return START_STICKY;
    }
}
