package com.example.hpmak.android_doradca_zywieniowy.Adapter;

import com.example.hpmak.android_doradca_zywieniowy.Model.Advice;
import java.util.List;

public interface IDatabaseAdapter {
    //--------------------------ADD------------------------------------------------------------------------------
    boolean addDataTableAdvice(Advice advice, String idUser);

    //--------------------------GET------------------------------------------------------------------------
    List<Advice> getAllDataTableAdvice();
    List<Advice> getDataTableAdvice(String idUser);
    Advice getAdvice(String id, String idUser);

    //--------------------------UPDATE------------------------------------------------------------------------------
    boolean updateDataTableAdvice(Advice advice, String idUser);

    //--------------------------DELETE------------------------------------------------------------------------------
    Integer deleteDataTableAdvice(String id, String idUser);

    void deleteAllData(String tablename);
    void deleteTable(String tablename);
    void insertToDB(String sql);
}
