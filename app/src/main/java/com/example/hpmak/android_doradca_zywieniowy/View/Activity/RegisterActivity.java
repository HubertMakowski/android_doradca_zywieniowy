package com.example.hpmak.android_doradca_zywieniowy.View.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hpmak.android_doradca_zywieniowy.Listener.OnCompleteListener;
import com.example.hpmak.android_doradca_zywieniowy.Presenter.RegisterActivityPresenter;
import com.example.hpmak.android_doradca_zywieniowy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity implements RegisterActivityPresenter.RegisterActivityView {

    private static String TAG = "RegisterActivity";

    @BindView(R.id.activity_register_et_name)
    EditText name;
    @BindView(R.id.activity_register_et_email)
    EditText email;
    @BindView(R.id.activity_register_et_password)
    EditText password;
    @BindView(R.id.activity_register_et_rePassword)
    EditText repassword;

    private RegisterActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        presenter = new RegisterActivityPresenter(this);
    }

    @OnClick(R.id.tv_activity_register_login_link)
    void goToLoginScreen() {
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @OnClick(R.id.btn_activity_register_sign_up)
    void registerUser() {
        boolean isCorrect = presenter.validateData();

        if(isCorrect) {
            presenter.registerUser(getUserEmail(), getUserPassword());
        }
    }

    @Override
    public void printMessage(String toast, String debug) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
        Log.d(TAG, debug);
    }

    @Override
    public void goToProfilScreen() {
        startActivity(new Intent(this, ProfileActivity.class));
        finish();
    }

    @Override
    public void showUserEmailError(int resId) {
        email.setError(getString(resId));
    }

    @Override
    public void showUserPasswordError(int resId) {
        password.setError(getString(resId));
    }

    @Override
    public void showUserRePasswordError(int resId) {
        repassword.setError(getString(resId));
    }

    @Override
    public String getUserDisplayName() {
        return name.getText().toString().trim();
    }

    @Override
    public String getUserEmail() {
        return email.getText().toString().trim();
    }

    @Override
    public String getUserPassword() {
        return password.getText().toString().trim();
    }

    @Override
    public String getUserRePassword() {
        return repassword.getText().toString().trim();
    }
}
