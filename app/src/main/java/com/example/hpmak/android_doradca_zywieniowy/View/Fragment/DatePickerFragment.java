package com.example.hpmak.android_doradca_zywieniowy.View.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.example.hpmak.android_doradca_zywieniowy.R;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private DatePickerFragmentListener listener;

    public interface DatePickerFragmentListener {
        void setTvDateOfBirth(String date);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DatePickerFragmentListener){
            listener = (DatePickerFragmentListener) context;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2000);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog((Activity)listener, this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        listener.setTvDateOfBirth(day + " " + intToStringDate(month) + " " + String.valueOf(year));
    }

    private String intToStringDate(int month) {
        switch(month) {
            case 0:
                return getResources().getString(R.string.January);
            case 1:
                return getResources().getString(R.string.February);
            case 2:
                return getResources().getString(R.string.March);
            case 3:
                return getResources().getString(R.string.April);
            case 4:
                return getResources().getString(R.string.May);
            case 5:
                return getResources().getString(R.string.June);
            case 6:
                return getResources().getString(R.string.July);
            case 7:
                return getResources().getString(R.string.August);
            case 8:
                return getResources().getString(R.string.September);
            case 9:
                return getResources().getString(R.string.October);
            case 10:
                return getResources().getString(R.string.November);
            case 11:
                return getResources().getString(R.string.December);
            default:
                return "error";
        }
    }
}