package com.example.hpmak.android_doradca_zywieniowy.Presenter;

import com.example.hpmak.android_doradca_zywieniowy.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegisterActivityPresenterTest {

    @Mock
    private RegisterActivityPresenter.RegisterActivityView view;
    @Mock
    private RegisterActivityPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new RegisterActivityPresenter(view);
    }

    @Test
    public void validateEmail() throws Exception {
        when(view.getUserEmail()).thenReturn("");
        presenter.validateData();
        verify(view).showUserEmailError(R.string.user_email_error);
    }

    @Test
    public void validatePassword() throws Exception {
    }
}